// Copyright (c) Microsoft and Contributors. All rights reserved. Licensed under the University of Illinois/NCSA Open Source License. See LICENSE.txt in the project root for license information.

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using ClangSharp.Interop;

namespace ClangSharp.Abstractions
{
    internal struct RecordDesc<TCustomAttrGeneratorData>
    {
        public string EscapedName { get; set; }
        public string NativeType { get; set; }
        public string NativeInheritance { get; set; }
        public StructLayoutAttribute Layout { get; set; }
        public Guid? Uuid { get; set; }
        public StructFlags Flags { get; set; }
        public CXSourceLocation? Location { get; set; }

        public bool HasVtbl
        {
            get
            {
                return (Flags & StructFlags.HasVtbl) != 0;
            }

            set
            {
                Flags = value ? Flags | StructFlags.HasVtbl : Flags & ~StructFlags.HasVtbl;
            }
        }

        public Action<TCustomAttrGeneratorData> WriteCustomAttrs { get; set; }
        public TCustomAttrGeneratorData CustomAttrGeneratorData { get; set; }
        public long Size32 { get; set; }
        public long Size64 { get; set; }
        public RecordKind Kind { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal enum RecordKind
    {
        @struct,
        @interface,
        union
    }
}
