// Copyright (c) Microsoft and Contributors. All rights reserved. Licensed under the University of Illinois/NCSA Open Source License. See LICENSE.txt in the project root for license information.

using ClangSharp.Interop;

namespace ClangSharp.Abstractions
{
    internal struct FieldDesc
    {
        public string NativeTypeName { get; set; }
        public string EscapedName { get; set; }
        public long? Offset { get; set; }
        public bool NeedsNewKeyword { get; set; }
        public string InheritedFrom { get; set; }
        public CXSourceLocation? Location { get; set; }
    }
}
