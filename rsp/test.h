
#include <windows.h>
#include <ddraw.h>

extern "C" {

//typedef LRESULT (__stdcall* TESTCB)(HWND, UINT, WPARAM, LPARAM);

typedef LRESULT (__attribute__((stdcall)) fun_type_test)(HWND, UINT, WPARAM, LPARAM);

typedef struct sasi {
    LRESULT (__attribute__((stdcall)) * lpfnTestCb)(HWND, UINT, WPARAM, LPARAM);
    fun_type_test* funptr;
} SASI, *PSASI;

typedef struct yunyun {
    union {
        const struct {
            int a;
            char b;
        };
        struct {
            char c;
            char d;
        };
    };
} YUNYUN, *PYUNYUN;

typedef enum TEST_ENUM {
    TestEnumValue1 = 1,
    TestEnumValue2,
    TestEnumValue3,
    TestEnumValue4,
    TestEnumValue5,
    TestEnumValue6,
    TestEnumValue7,
} TEST_ENUM, *PTEST_ENUM;

typedef struct arrays {
    BYTE dim1[8];
    BYTE dim2[8][16]; // TODO: uncomment when implemented
} arrays;

typedef const int constint;
typedef constint noconstint;
typedef int intint;

typedef struct consts {
    const constint a;
    const intint b;
    const const int c;
    const noconstint d;
} consts;

VOID CALLBACK TestCb(void);
VOID WINAPI TestVoid(void);
void WINAPIV TestVararg(int a, ...);
VOID WINAPI TestPtr(int const * * const * ptr, int const * * const * const constPtr);
VOID TestArray(const int array[]);
VOID TestArray2(const int array[3][7]);
VOID TestConst(const int c, int nc);

#undef INTERFACE
#define INTERFACE ITestIface
DECLARE_INTERFACE_( ITestIface, IUnknown )
{
    /*** IUnknown methods ***/
    STDMETHOD(QueryInterface) (THIS_ REFIID riid, LPVOID FAR * ppvObj) PURE;
    STDMETHOD_(ULONG,AddRef) (THIS)  PURE;
    STDMETHOD_(ULONG,Release) (THIS) PURE;
    /*** IDirectDraw methods ***/
    STDMETHOD(Hello1)(THIS) PURE;
    STDMETHOD(Hello2)(THIS_ DWORD, LPVOID stuff, VOID* anotherStuff) PURE;
};

}